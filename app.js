angular.module('paintApp',['ngRoute','PaintControllers']).
config(['$routeProvider',function($routeProvider){
 $routeProvider.
  when('/',{
    templateUrl: 'layout/home.html',
    controller: 'HomeCtrl'
  }).
  when('/about',{
    templateUrl: 'layout/about.html',
    controller: 'AboutCtrl'
  }).
  when('/detail/:id',{
    templateUrl: 'layout/detail.html',
    controller: 'DetailCtrl'
  }).
  when('/service',{
    templateUrl: 'layout/service.html',
    controller: 'ServiceCtrl'
  }).
  otherwise({
    redirectTo: 'about.html'
  }); 
}]);

var app = angular.module('PaintControllers', []);

app.controller('HomeCtrl', ['$scope', '$http','blog', function ($scope, $http,blog) {
  blog.getPosts().then(function(posts){
    $scope.posts = posts;
  })
}]);

app.controller('AboutCtrl', ['$scope', 
  function($scope){
    $scope.page="About Controller."
}]);

app.controller('ServiceCtrl',['$scope',
  function($scope){
    $scope.page = "We are in Service Controller "
}]);
app.factory('blog',['$http','$q',function($http,$q){
  function getPosts() {
    var def = $q.defer();
    $http.get("http://localhost:3000/posts")
    .success(function(data) {
      def.resolve(data);
    })
    .error(function() {
      def.reject("Failed to get albums");
    });
    return def.promise;
  }
  function getPostById(post_id) {
    var def = $q.defer();
    $http.get("http://localhost:3000/posts/"+post_id)
    .success(function(data) {
      def.resolve(data);
    })
    .error(function() {
      def.reject("Failed to get albums");
    });
    return def.promise;
  }
  return  {
    getPosts:getPosts,
    getPostById:getPostById
  }
}]);
app.controller('DetailCtrl',['$scope','blog','$routeParams', function($scope,blog,$routeParams){
  blog.getPostById($routeParams.id).then(function(post){
    $scope.post = post;
  })
}]);

